package Test;

import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Natanon Poonawagul
 *
 */
public class StackTest {
	private Stack stack;
	
	@Before
	public void setNewStack(){
		//setStackType(0,1)
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(5);
	}
	@Test( expected=java.util.EmptyStackException.class )
	public void testEmptyStack() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0,stack.size());
		assertNull(stack.peek());
		stack.pop();
	}
	@Test
	public void testPush(){
		stack.push("apple");
		assertNotNull(stack.peek());
		assertFalse(stack.isEmpty());
		assertEquals(1,stack.size());
		assertNotNull(stack.pop());
		assertTrue(stack.isEmpty());
		assertEquals(0,stack.size());
	}
	@Test( expected=IllegalStateException.class )
	public void testFull(){
		assertTrue(stack.isEmpty());
		String [] fruits = {"apple","banana","coconut","durian","pineapple"};
		for(String fruit : fruits){
			stack.push(fruit);
			assertNotNull(stack.peek());
			assertFalse(stack.isEmpty());
		}
		assertEquals(5,stack.size());
		assertTrue(stack.isFull());
		stack.push("morefruit");
	} 
	@Test
	public void testEqualPop(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		assertEquals("apple",stack.pop());
		stack.push("banana");
		stack.push("coconut");
		assertEquals("coconut",stack.pop());
	}
	@Test( expected=IllegalArgumentException.class )
	public void testPushNull(){
		stack.push(null);
	}
	@Test
	public void testEqualPeek(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		assertEquals("apple",stack.peek());
		stack.push("banana");
		stack.push("coconut");
		assertEquals("coconut",stack.peek());
	}
	@Test
	public void testSizePop(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		stack.push("banana");
		stack.push("coconut");
		stack.pop();
		assertEquals(2,stack.size());
		stack.pop();
		assertEquals(1,stack.size());
		stack.pop();
		assertEquals(0,stack.size());
		assertTrue(stack.isEmpty());
	}
	@Test
	public void testSizePeek(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		stack.push("banana");
		stack.push("coconut");
		stack.peek();
		assertEquals(3,stack.size());
	}
	@Test
	public void testEqualFull(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		stack.push("banana");
		stack.push("coconut");
		assertTrue(stack.isFull());
		assertEquals(stack.size(),stack.capacity());
	}
	@Test
	public void TestEqualPopAndPeek(){
		stack = StackFactory.makeStack(3);
		stack.push("apple");
		assertEquals("apple",stack.peek());
		stack.push("banana");
		stack.push("coconut");
		assertEquals("coconut",stack.pop());
		assertEquals("banana",stack.peek());
		assertEquals("banana",stack.pop());
		assertEquals("apple",stack.peek());
	}

}
